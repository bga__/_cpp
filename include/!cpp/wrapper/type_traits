#pragma once

#if !defined(__IAR_SYSTEMS_ICC__ )
	#include <type_traits>
#else

#include <!cpp/wrapper/cstdint>

namespace std {
struct true_type {
	enum { value = true };
};
struct false_type {
	enum { value = false };
};

template<class T1, class T2> struct is_same : public false_type {  };
template<class T> struct is_same<T, T> : public true_type {  };

template<bool isEnableArg, class TArg> struct enable_if;
template<class TArg> struct enable_if<true, TArg> {
	typedef TArg type;
};

template<bool isEnableArg, class TArg, class FArg> struct conditional;
template<class TArg, class FArg> struct conditional<true, TArg, FArg> {
	typedef TArg type;
};
template<class TArg, class FArg> struct conditional<false, TArg, FArg> {
	typedef FArg type;
};

template<class TArg, class RArg = void> struct enable_if_has_type {
	typedef RArg type;
};

template <class T> struct remove_const { typedef T type; };
template <class T> struct remove_const<const T> { typedef T type; };

template <class T> struct remove_volatile { typedef T type; };
template <class T> struct remove_volatile<volatile T> { typedef T type; };

template <class T> struct remove_cv { typedef typename remove_const<typename remove_volatile<T>::type>::type type; };

namespace details {

template <class T> struct intergal_trait;
template <class T> struct intergal_trait<const T> : public intergal_trait<T> { };
template <class T> struct intergal_trait<volatile T> : public intergal_trait<T> { };
template <class T> struct intergal_trait<volatile const T> : public intergal_trait<T> { };

#define BGA__DEF_INTERGAL_TRAIT(typeArg, signedTypeArg, unsignedTypeArg) \
	template <> struct intergal_trait<typeArg> { \
		typedef signedTypeArg signed_type;  \
		typedef unsignedTypeArg unsigned_type; \
	} \
;

#define BGA__DEF_INTERGAL_TRAIT2(signedTypeArg, unsignedTypeArg) \
	BGA__DEF_INTERGAL_TRAIT(signedTypeArg, signedTypeArg, unsignedTypeArg) \
	BGA__DEF_INTERGAL_TRAIT(unsignedTypeArg, signedTypeArg, unsignedTypeArg) \
;


BGA__DEF_INTERGAL_TRAIT(char, signed char, unsigned char)
BGA__DEF_INTERGAL_TRAIT2(signed char, unsigned char)
BGA__DEF_INTERGAL_TRAIT2(signed short, unsigned short)
BGA__DEF_INTERGAL_TRAIT2(signed int, unsigned int)
BGA__DEF_INTERGAL_TRAIT2(signed long, unsigned long)
#ifdef UINT64_MAX
	BGA__DEF_INTERGAL_TRAIT2(int64_t, uint64_t)
#endif

#undef BGA__DEF_INTERGAL_TRAIT2
#undef BGA__DEF_INTERGAL_TRAIT

} //# namespace

template <class T> struct make_signed { typedef typename details::intergal_trait<T>::signed_type type; };
template <class T> struct make_unsigned { typedef typename details::intergal_trait<T>::unsigned_type type; };

template <class T, class Enable = void> struct is_integral : public false_type {  };
template <class T> struct is_integral<T, typename enable_if_has_type<typename details::intergal_trait<T>::unsigned_type >::type> : public true_type {  };

// template <class T, class Enable = void> struct is_signed : public false_type {  };
template <class T, class Enable = void> struct is_signed;
template <class T> struct is_signed<T, typename enable_if_has_type<typename details::intergal_trait<T>::unsigned_type >::type> { enum { value = is_same<typename remove_cv<T>::type, typename details::intergal_trait<T>::signed_type>::value }; };

// template <class T, class Enable = void> struct is_unsigned : public false_type {  };
template <class T, class Enable = void> struct is_unsigned;
template <class T> struct is_unsigned<T, typename enable_if_has_type<typename details::intergal_trait<T>::unsigned_type >::type> { enum { value = is_same<typename remove_cv<T>::type, typename details::intergal_trait<T>::unsigned_type>::value }; };

} //# namespace std
#endif
